# Game Title: Exit Dark Dungeon
Repository berisi Code menggunakan Godot Engine untuk membentuk permainan Exit Dark Dungeon.
Code dibuat secara rapid prototyping untuk keperluan pembelajaran dalam membuat permainan.
## Apa Itu Exit Dark Dungeon?
Exit Dark Dungeon adalah Permainan 2 dimensi yang cepat dimainkan. Permainan dibuat sulit dengan damage yang diberikan oleh musuh. Terdapat 2 kategori musuh, statik dan dinamik. Musuh statik akan berdiam ditempat dan mencoba menyerang, sedangkan dinamik musuh bergerak mendekati player dan mencoba menyerang.

Design permainan dimulai dengan ide suasana gelap didalam goa. Environment goa diubah menjadi dungeon. Untuk memberikan cahaya jalanan, setiap objek memiliki energi yang memancarkan cahaya. Pada godot implementasi menggunakan light2D.

Objek pada permainan adalah pelengkap agar player memperoleh tantangan dan merasa berada pada dungeon. Musuh mengimplementasikan code menembak secara otomatis atau berjalan mendekati player. Tujuan untuk belajar script pada godot.

## Tujuan pada permainan
Objective pada permainan adalah mengumpulkan objek(5 coins) dan bertanding berdasarkan waktu. Karena permainan offline dan single player, diharapkan menggunakan metode lain untuk menunjukkan score terbaik kalian.

## Assets
assets gambar diperoleh pada:
- [Kenney](https://www.kenney.nl/)
- [freepik](https://www.freepik.com/)
- [pngwing](https://www.pngwing.com/)
- [pngegg](https://www.pngegg.com/)

BGM diperoleh pada 
- [chosic](https://www.chosic.com/)
,Special thanks to Loyalty Freak Music and John Bartmann
  
Aplikasi
- Godot Engine
- Audacity

## Youtuber
Dalam pembejaran, kalian juga bisa mengecek channel youtuber luar biasa sesama game developer:
- [bitbrain](https://www.youtube.com/c/bitbraindev)
- [BornCG](https://www.youtube.com/c/BornCG)
- [Code with Tom](https://www.youtube.com/c/CodewithTom)
- [UmaiPixel](https://www.youtube.com/c/UmaiPixel)
- [Rungeon](https://www.youtube.com/channel/UCEtKeMUvtI-2eXVY8hg7nHw)


