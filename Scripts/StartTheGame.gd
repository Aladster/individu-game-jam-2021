extends LinkButton

export(String) var scene_to_load

func _on_StartTheGame_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	global.timer_on=true
	global.holding_another_weapon = false
	global.HealthPoint = 100
	global.Ammo = 0
	global.BurningStatus = false
	global.ElectrocutedStatus = false
	global.coins = 0
	global.time = 0
	
	global.total_achivement = 0
	global.slime_kill = 0
	global.slime_kill_achivement = false
	global.to_much_medic = false
	global.bullet_hit_player = 0
	global.dodge_bullet_achivement = false
	global.speedrun_achivement = false
	global.total_empty_shell_shot = 0
	global.invisible_bullet_achivement = false
