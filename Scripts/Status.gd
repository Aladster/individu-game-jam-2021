extends Label

func _process(delta):
	if global.HealthPoint>100:
		add_color_override("font_color", "#24BADF")
	if global.HealthPoint<=100:
		add_color_override("font_color", "#28EB0A")
	if global.HealthPoint<70:
		add_color_override("font_color", "#D1D100")
	if global.HealthPoint<25:
		add_color_override("font_color", "#870714")

	self.text = "Hp : " + str(global.HealthPoint)

