extends RigidBody2D

var explosion = preload("res://Scenes/BulletType/LightningEffect.tscn")

func _process(delta):
	$AnimatedSprite.play("Walk")


func _on_RigidBody2D_body_entered(body):
	if "Player" in body.name:
		var explosion_instance = explosion.instance()
		explosion_instance.position = get_global_position()
		get_tree().get_root().add_child(explosion_instance)
		body.hit(self)
	
	if !body.is_in_group("StaticTurret"):
		queue_free()
