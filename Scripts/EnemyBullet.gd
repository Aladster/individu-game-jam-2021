extends RigidBody2D

var explosion = preload("res://Scenes/Explosion.tscn")

func _on_EnemyBullet_body_entered(body):
	if "Player" in body.name:
		var explosion_instance = explosion.instance()
		explosion_instance.position = get_global_position()
		get_tree().get_root().add_child(explosion_instance)
		body.hit(self)
		
	if !body.is_in_group("EnemySlime"):
		queue_free()
