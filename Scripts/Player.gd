extends KinematicBody2D

export var speed = 75
export var bullet_speed = 1000
export var fire_rate = 0.6

var bullet = preload("res://Scenes/Bullet.tscn")
var can_fire = true

var burning=0

func hit(body):
	global.bullet_hit_player+=1
	if "Fireball" in body.name:
		$Burning.play()
		global.BurningStatus=true
		global.HealthPoint-=10
		burning=5
		$Timer.start()
				
	if "LightningBolt" in body.name:
		$Electrocuted.play()
		global.ElectrocutedStatus=true
		global.HealthPoint-=20
		speed = 15
		$Timer2.start()
	if "EvilBullet" in body.name:
		$EvilBullet.play()
		global.HealthPoint-=70
	pass

func gun():
	var bullet_instance = bullet.instance()
	bullet_instance.position = $BulletPoint.get_global_position()
	bullet_instance.rotation_degrees = rotation_degrees
	bullet_instance.apply_impulse(Vector2(), Vector2(bullet_speed, 0).rotated(rotation))
	get_tree().get_root().add_child(bullet_instance)
	can_fire = false
	$Pew.play()
	yield(get_tree().create_timer(fire_rate), "timeout")
	can_fire = true
		
func _process(delta):
	look_at(get_global_mouse_position())
	
	if Input.is_action_pressed("fire") and can_fire:
		if global.holding_another_weapon:
			if global.Ammo>0:
				gun()
				global.Ammo-=1
			else:
				global.total_empty_shell_shot+=1
				$NoAmmo.play()
		else:
			gun()
	
func _physics_process(delta):
	var direction = Vector2()
	if Input.is_action_pressed("ui_up"):
		direction += Vector2(0, -1)
	if Input.is_action_pressed("ui_down"):
		direction += Vector2(0, 1)
	if Input.is_action_pressed("ui_left"):
		direction += Vector2(-1, 0)
	if Input.is_action_pressed("ui_right"):
		direction += Vector2(1, 0)
	if Input.is_action_just_pressed("switch"):
		if !global.holding_another_weapon:
			fire_rate = 0.1
			global.holding_another_weapon=true
		else:
			fire_rate = 0.6
			global.holding_another_weapon=false
			
	move_and_slide(direction * speed)


func _on_Timer_timeout():
	burning-=1
	global.HealthPoint-=6
	$Timer.stop()
	if burning>0:
		$Timer.start()
	else:
		global.BurningStatus=false


func _on_Timer2_timeout():
	speed = 75
	$Timer2.stop()
	global.ElectrocutedStatus=false


func _on_Area2D_body_entered(body):
	if "EnemyChase" in body.name:
		$Timer3.start()
	pass


func _on_Timer3_timeout():
	global.bullet_hit_player+=1
	global.HealthPoint-=5
	$Timer3.stop()
	$Timer3.start()
	
