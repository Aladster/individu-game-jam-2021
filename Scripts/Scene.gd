extends Node2D

export(String) var scene_to_load

func _process(delta):
	if (global.timer_on):
		global.time += delta
		
	if global.HealthPoint<1:
		global.timer_on = false
		get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	
	if global.coins==5:
		global.timer_on = false
		get_tree().change_scene(str("res://Scenes/Scene/Win.tscn"))
