extends KinematicBody2D

export var bullet_speed = 200
export var fire_rate = 1

export var bullet_type = "Fireball"
var bullet = preload("res://Scenes/EvilBullet.tscn")
var Fireball = preload("res://Scenes/BulletType/Fireball.tscn")
var Lightning = preload("res://Scenes/BulletType/LightningBolt.tscn")

var can_fire = true
var player = null

var move = Vector2.ZERO
var speed = 1.2
var is_dead = false
var health=2
var bullet_instance = null

func hit():
	health-=1
	if health==0:
		is_dead = true
		global.slime_kill+=1
		move = Vector2.ZERO
		$CollisionShape2D.disabled = true
		$AnimatedSprite.play("Dead")
		$Timer.start()
	pass

func gun():
	if bullet_type=="EvilBullet":
		bullet_instance = bullet.instance()
	elif bullet_type=="Fireball":
		bullet_instance = Fireball.instance()
	elif bullet_type=="Lightning":
		bullet_instance = Lightning.instance()
	bullet_instance.position = $BulletPoint.get_global_position()
	bullet_instance.rotation_degrees = rotation_degrees
	bullet_instance.apply_impulse(Vector2(), Vector2(0, -bullet_speed).rotated(rotation))
	get_tree().get_root().add_child(bullet_instance)
	can_fire = false
	yield(get_tree().create_timer(fire_rate), "timeout")
	can_fire = true
	
	
func _ready():
	if bullet_type=="EvilBullet":
		$Light2D.set_color("#0c3824")
		$Light2D2.set_color("#0c3824")
	elif bullet_type=="Fireball":
		$Light2D.set_color("#260c38")
		$Light2D2.set_color("#260c38")
	elif bullet_type=="Lightning":
		$Light2D.set_color("#43460e")
		$Light2D2.set_color("#43460e")
		
		
func _process(delta):
	if !is_dead:
		if player !=null:
			look_at(player.position - global_position)
	
		if player!=null&&can_fire:
			gun()
		
func _physics_process(delta):
	if !is_dead:
		move = Vector2.ZERO
	
		if player!=null:
			move = position.direction_to(player.position) * speed
			$AnimatedSprite.play("Walk")
		else:
			move = Vector2.ZERO
			$AnimatedSprite.play("Idle")
	
		move = move.normalized()
		move = move_and_collide(move)
	
	
func _on_Area2D_body_entered(body):
	if "Player" in body.name:
		player = body

func _on_Area2D_body_exited(body):
	if "Player" in body.name:
		player = null

func _on_Timer_timeout():
	queue_free()
