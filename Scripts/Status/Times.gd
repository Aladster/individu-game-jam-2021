extends Label

var mins = 0
var secs = 0

func _process(delta):
	secs = fmod(global.time,60)
	mins = fmod(global.time,60*60)/60
	
	var time_passed = "%02d : %02d" %[mins,secs]
	self.text = "Times : " + time_passed
