extends Label

func _process(delta):
	if global.holding_another_weapon:
		self.text = "Ammo : " + str(global.Ammo) + "     holding machinegun"
	else:
		self.text = "Ammo : Unlimited     holding handgun"
